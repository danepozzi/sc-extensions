NoiseTest {
	var numChannels=2, noiseAmp= -30, noiseBody=0.5, noiseFade=0.2, whichServer;

	*new { arg chans=2,amp= -30,body=0.5,fade=0.2,server;
		^super.new.init(chans,amp,body,fade,server)
	}

	init {
		arg chans,amp,body,fade,server;

		numChannels=chans;
		noiseAmp=amp;
		noiseBody=body;
		noiseFade=fade;
		whichServer=server;

		loop{numChannels.do{
			arg i; {
				Out.ar(
					i,
					EnvGen.kr(
						Env.linen(noiseFade,noiseBody,noiseFade,1),
						doneAction:2
					)*WhiteNoise.ar(noiseAmp.dbamp)
				)
			}.play(whichServer);
			(noiseBody+(noiseFade*2)).wait;
		}}
	}
}


//Task({NoiseTest(12, -50, server:s)}).play