RingBuf {
	var mono=true, bus= 400, dur=10, update=5, posx=1500, posy=400, <>buf;

	*initClass {
		StartUp.add {
			this.initSynthDefs;
		}
	}

	*initSynthDefs {
		SynthDef(\ringbufmono, {
			arg buffer, bus;
			RecordBuf.ar(InFeedback.ar(bus), buffer, loop: 1);
		}).add;

		SynthDef(\ringbufstereo, {
			arg buffer, bus;
			RecordBuf.ar(InFeedback.ar([bus,bus+1]), buffer, loop: 1);
		}).add;
	}

	*new {
		arg isMono=true, firstbus=400, duration=10, updatePeriod=5, px=1500, py=400;
		^super.new.init(isMono,firstbus,duration,updatePeriod,px,py)
	}

	init {
		arg isMono, firstbus, duration, updatePeriod, px, py;
		var w, x, r, v;

		mono = isMono;
		bus = firstbus;
		dur = duration;
		update = updatePeriod;
		posx = px;
		posy = py;
		if(mono==true){
			buf = Buffer.alloc(Server.default, 44100*dur, 1);
			buf.postln;
		}{
			buf = Buffer.alloc(Server.default, 44100*dur, 2);
			buf.postln;
		};
		w = Window.new("",Rect(posx, posy, 300, 150)).front.alwaysOnTop_(true);
		v = UserView(w, Rect(0,0,300,150));

		Button(w, Rect(300-30,150-30,30,30)).states_([
			["OFF", Color.white, Color.black],
			["ON", Color.white, Color.red]
		]).action_({
			arg butt;
			if(butt.value==1){
				if(mono==true){
					\mono.postln;
					r = Synth(\ringbufmono, [buffer: buf, \bus, bus]);
					x = Routine({ { defer{buf.plot(parent:v)}; 5.wait }.loop }).play;
				}{
					\stereo.postln;
					r = Synth(\ringbufstereo, [buffer: buf, \bus, bus]);
					x = Routine({ { defer{buf.plot(parent:v)}; 5.wait }.loop }).play;
				}
			}{
				x.stop;
				x.clear;
				r.free;
			};
		});
	}
}