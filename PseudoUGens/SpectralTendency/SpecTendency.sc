SpecTendency {
	*ar {
		arg in, numsamp = 40, coeff = 1;
		var cutoff = LocalIn.ar(1, 25);
		var sig = in;
		var lpf = LPF.ar(sig, cutoff);
		var hpf = HPF.ar(sig, cutoff);
		var lrms = RunningSum.rms(lpf, numsamp);
		var hrms = RunningSum.rms(hpf, numsamp);
		var tendency = hrms - lrms;
		tendency = Integrator.ar(tendency,coeff);
		LocalOut.ar(tendency);
		^tendency;
	}
}