KeySampler {
	var length=20, key=18, posx=1500, posy=400, <>buffer;

	*initClass {
		StartUp.add {
			this.initSynthDefs;
		}
	}

	*initSynthDefs {

		SynthDef(\sampler,{
			arg startRec = 0, endRec = 0, buf, which;
			var sig= SoundIn.ar(0);
			var wrHead= Phasor.ar(0, BufRateScale.kr(buf), 0, BufFrames.kr(buf));
			var end= Latch.ar(wrHead, endRec);
			var start= Latch.ar(wrHead, startRec);
			var sampled;
			BufWr.ar(LPF.ar(sig,15), buf, wrHead);
			SendReply.kr(endRec, '/sampler', [start, end, which]);
			//sampled= BufRd.ar(1, x, Phasor.ar(0, BufRateScale.kr(b), start, end), 1, 2);
			//Out.ar(0, Limiter.ar(sampled, 0.1));
			0;
		}).add;
	}

	*new {
		arg maxLength=20, keyValue=18, px=1500, py=400, normalize = false;
		^super.new.init(maxLength,keyValue,px,py,normalize)
	}

	init {
		arg maxLength,keyValue,px,py,normalize;
		var b = Buffer.alloc(Server.local, maxLength*44100, 1), w, f, t, o;

		length = maxLength;
		key = keyValue;
		posx = px;
		posy = py;
		buffer= Buffer.alloc(Server.local,44100,1);
		w = Window("press:" ++ (key-17), Rect(posx,posy,300,150)).front.alwaysOnTop_(true);

		/*findNextSC = {
			arg buffer, start, end;
			var value;

			buffer.loadToFloatArray(action: {
				arg array;
				var a = array;
				while({a[start] > 0}, {
					a[start];
					start = start + 1;
				});
				while({a[start] < 0}, {
					a[start];
					start = start + 1;
				});
				while({a[end] > 0}, {
					a[end];
					end = end + 1;
				});
				while({a[end] < 0}, {
					a[end];
					start = end + 1;
				});

				buffer.free;
				buffer= Buffer.alloc(Server.local,bufferLength,1);
				b.copyData(buffer,b.bufnum,start,bufferLength);
				buffer.normalize;
				defer{buffer.plot(parent:w)};
			});
		};*/

		f= {
			arg start, end, totalSamples, which;
			var bufferLength;
			if(which==key){
				if (end>start){bufferLength= end-start}{bufferLength= totalSamples-start+end};
				buffer.free;
				buffer= Buffer.alloc(Server.local,bufferLength,1);
				b.copyData(buffer,b.bufnum,start,bufferLength);
				if(normalize){buffer.normalize};
				defer{buffer.plot(parent:w)};
			}
		};

		o = OSCdef(("sampler" ++ key).asSymbol, {|msg| f.(msg[3],msg[4], maxLength*44100, msg[5])}, '/sampler').permanent_(true);

		t = Synth(\sampler, [buf: b, which: key]);
		CmdPeriod.add({SystemClock.sched(0.1, {
			t = Synth(\sampler, [buf: b, which: key]);
			//b = Buffer.alloc(Server.local, maxLength*44100, 1);
			//buffer= Buffer.alloc(Server.local,44100,1);
		})});

		w.view.keyDownAction_({ arg view,char,modifiers,unicode,keycode;
			switch(keycode){ key } {
				t.set(\startRec, 1, \endRec, 0);
			};
		});

		w.view.keyUpAction_({ arg view,char,modifiers,unicode,keycode;
			switch(keycode){ key } {
				t.set(\endRec, 1, \startRec, 0);
			};
		});

		w.onClose_({t.free;b.free;buffer.free;o.clear});
	}
}