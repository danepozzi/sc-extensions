Scoper {
	var width, height;

	*new {
		arg awidth, aheight;
		^super.new.init(awidth,aheight)
	}

	init {
		arg awidth, aheight;
		var xfreq= 0, steps= 11, steps2= 8, initfreq=44100, initdb= -96, db=0;
		var passo, passo2, window, scoper;
		width= awidth; height= aheight;
		passo= (width-(xfreq))/steps;
		passo2= (height-15)/(steps2+1);
		window = Window("", Rect(1040, 590, width, height), border:true); // width should be 511
		scoper = FreqScopeView(window, Rect(xfreq, 20, width-(xfreq), height-20));
		scoper.waveColors = [Color.black];
		window.background= Color.black.alpha = 0.15;
		steps.do({initfreq=initfreq/2});
		initdb= initdb/steps2;
		scoper.background= Color.black.alpha = 0.15;
		scoper.active_(true); // turn it on the first time;
		scoper.freqMode_(1);

		StaticText(window, Rect(xfreq, 0, 50, 20))
		.font_(Font([
			"Helvetica-Bold",
			"Helvetica",
			"Monaco",
			"Arial",
			"Gadget",
			"MarkerFelt-Thin"
		].choose, 7)).string = 0;
		steps.do({
			arg i;
			var xpointer= xfreq+(passo*(i+1));
			var freq= StaticText(window, Rect(xpointer, 0, 50, 20));
			initfreq= initfreq*2;
			freq.font_(Font([
				"Helvetica-Bold",
				"Helvetica",
				"Monaco",
				"Arial",
				"Gadget",
				"MarkerFelt-Thin"
			].choose, 7));
			freq.string = initfreq.round;
		});

		window.onClose_({ scoper.kill }); // you must have this
		window.front.alwaysOnTop_(true);
	}
}