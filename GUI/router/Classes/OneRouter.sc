OneRouter {
	var bus=100, numOutputs=12;

	*new {
		arg busNum=100, outputs=2;
		^super.new.init(busNum,outputs)
	}

	init {
		arg busNum, outputs;
		var sends = List.new;
		var buttons = Array.newClear(numOutputs);
		var e,w,b;

		bus = busNum;
		numOutputs = outputs;
		e = topEnvironment;
		w = Window.new("one",Rect(0, 0, 20*numOutputs+30, 20)).front.alwaysOnTop_(true);

		b = NumberBox(w, Rect(0, 0, 30, 20))
		.background_(Color.black.alpha = 0.15)
		.normalColor_(Color.white.alpha = 0.7)
		.action_({ arg box;
			var setting = {
				"Setting routing for bus ".post;
				box.value.postln;
				buttons.do({arg butt; butt.value = 0});
			};
			var loading = {
				"Loading bus ".post;
				box.value.postln;
				buttons.do({arg butt, i; butt.value = sends[sends.asArray.find([bus])+i+1]});
			};
			bus = box.value;
			if (sends.size == 0){
				setting.value;
			} {
				if (sends[0] != bus) {
					block { |break|
						while {
							sends.includes(bus).if {
								loading.value;
								break.(bus)
							}{
								setting.value;
								false;
							}
						}
					};
				} {
					loading.value
				}
			}
		});
		b.value = 100;

		numOutputs.do({
			arg i;
			buttons[i] = Button(w, Rect(20*i+30, 0, 20, 20))
			.states_([
				[i, Color.white.alpha = 0.65, Color.grey],
				[i, Color.grey.alpha = 0.65, Color.green],
			])
			.action_({ arg butt;
				"Speaker#".post;
				i.post;
				" ".post;
				butt.value.postln;
				if (sends.size == 0){
					25.do({sends.add(0)});
					sends[0] = bus;
				} {
					block { |break|
						while {
							sends.includes(bus).if { break.(b) }{ true }
						}{
							25.do({sends.add(0)});
							sends[sends.size-25] = bus;
						}
					}
				};
				sends[sends.asArray.find([bus])+i+1] = butt.value;
				if (butt.value==1) {
					e[("rout" ++ bus ++ "ch" ++ i).asSymbol] = Synth(\default)
				} {
					e[("rout" ++ bus ++ "ch" ++ i).asSymbol].free
				};
			});
		});
		w.front;
	}
}