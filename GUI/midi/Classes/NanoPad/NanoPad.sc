NanoPad {
	var <>xy;

	*new {
		^super.new.init();
	}

	init {
		var nano_port = MIDIIn.findPort("nanoPAD2", "PAD");
		xy= Bus.control(numChannels:24);

		if(MIDIIn.findPort("nanoPAD2", "PAD").notNil){
			\xy_found.postln;

			MIDIdef.cc(\np_x, {arg val, num, chan;
				xy.setAt(0, (val/127).postln);
			}, (1), srcID: nano_port.uid).permanent_(true);

			MIDIdef.cc(\np_y, {arg val, num, chan;
				xy.setAt(1, (val/127).postln);
			}, (2), srcID: nano_port.uid).permanent_(true);
		};
	}
}